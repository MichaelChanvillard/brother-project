import React from 'react';

export default function DisplayMessage({dataMessages}){
    return(
        <div>
            {
                dataMessages.map((message,i)=>{
                    return (
                        // key est un container qui contient tous les contents de la catégorie message d'une personne
                        <div key={i}>
                            <p>Message {i+1} : {message.content}</p>
                            <p>Date : {message.date}</p>     
                        </div>
                    )
                })
            }
        </div> 
    );
};