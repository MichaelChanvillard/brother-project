import React, {useState} from 'react';

//data est une props
export default function SearchPeople({data}){
    const[query,setQuery] = useState('');
    
    //(event) = fonction anonyme qui prend event comme paramètre
    // à chaque fois que je saisis une lettre, chargement du DOM, changement valeur du state.
    const handleInputChange = (event)=> {
        const copieQuery = event.target.value;
        //1er état du query/state : on récupère ''
        //2ème état du query/state : on récupère la 1ère lettre
        const filterForInput = () => {
            //ma variable est vide
            let result 
            //filtrer le json : on boucle sur le tableau du json. user = un élément du tableau sur lequel on boucle
            let resultList = data.filter((user)=> {//retroune true ou false
                //tous les users inclus vont être ajoutés dans resultList
                return user.name.first.toLowerCase().includes(query.toLowerCase())||user.name.last.toLowerCase().includes(query.toLowerCase())
            })
            console.log(resultList)
        }
        
        setQuery(copieQuery);
        filterForInput();
    }

    return(
        <form>
            <input
                placeholder="Noter un nom"
                onChange={
                    (event)=>
                    handleInputChange(event)}
            />
                <p>{query}</p>
                              
        </form>
    )
}



//ne pas oublier de noter dans App.js l'endroit où je veux positionner ma barre de recherche.