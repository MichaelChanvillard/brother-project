import React from 'react';
import PeopleItem from './PeopleItem';

function ListPeoples({data}){
   return(
      data.map((element,index) =>{

         return (
            // dataElement est un props (permet de stocker un élément)
            // ={} permet d'affecter l'element du .map
            // key : react le nécessite quand on fait un map
            <PeopleItem dataElement={element} key={index}/>
         )    
      })
   )       
}

export default ListPeoples;