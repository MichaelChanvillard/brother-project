import React, {useState} from 'react';
import Collapse from 'react-bootstrap/Collapse';
import DisplayMessage from './DisplayMessage';

function PeopleItem (props) {
    const [open, setOpen] = useState(false);
    //[état actuel, fonction qui permet de modifier l'état]
    const [openMsg, setOpenMsg] = useState(false);

    return (
        <div className="row  border border-danger rounded my-2">
           <div className="m-4 ">
              <img onClick={() => setOpen(!open)} src={props.dataElement.picture} id={props.dataElement._id} alt="pictures people"
                 //les `` permettent de mettre du jsx
                 aria-controls={`example-collapse-text-${props.dataElement._id}`}
                 aria-expanded={open}
              ></img>
           </div>
           <div className="mx-4">
              <p>Nom :{props.dataElement.name.first}</p>
              <p>Prénon :{props.dataElement.name.last}</p>
              <p>Age :{props.dataElement.age}</p>
           </div>
           <Collapse in={open}>
              <div id={`example-collapse-text-${props.dataElement._id}`} className="mx-4">
                 <p>Adresse :{props.dataElement.address}</p>
                 <p>Email : {props.dataElement.email}</p>
                 <p>téléphone : {props.dataElement.phone}</p>
                 <p>Couleurs yeux : {props.dataElement.eyeColor}</p>
                 <p>Registre : {props.dataElement.registered}</p>
                 <p>A propos : {props.dataElement.about}</p>
                 {/* en cliquant sur le bouton, openMsg devient true car état initial false */}
                 <button onClick={() => setOpenMsg(!openMsg)}> Voir les messages</button>
                 {/* data va chercher les données du json qui sont dans la catégorie messages */}
                 {/* si openMsg est true alors affiche moi le composant DisplayMsg qui est l'enfant de PeopleItem  */}
                 {/* && est une condition */}
                 {/* message récupère un tableau avec des objets car ils sont entre {}*/}
                 {openMsg && <DisplayMessage dataMessages ={props.dataElement.messages}/>}
               </div>  
           </Collapse> 
      </div>  
     )    
}

export default PeopleItem;