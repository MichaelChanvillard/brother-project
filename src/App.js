import React from 'react';
import pentagram from './pentagram.png'
import './App.css';
import dataPeople from './data-json/data.json';
import ChoicePeople from './contenent/choicePeople';
import SearchPeople from './contenent/SearchPeople';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div>
          <div>
             <h1>Chasseur de vampire</h1>
          </div>
        </div>
        <img src={pentagram} className="App-logo" alt="logo" />

      </header>
      {/* je prends le fichier .json et je l'envoie les données */}
        <SearchPeople data = {dataPeople}/>
        <div id="list" className="container d-flex justify-content-center">
              <div className="col-10 ">
                <ChoicePeople data = {dataPeople}/>
              </div>
          </div> 
    </div>

  );
}

export default App;
